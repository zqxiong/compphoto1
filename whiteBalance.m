function J = whiteBalance(J)
    mean_J = mean(J,3);
    [~,index] = max(mean_J(:));
    [x,y] = ind2sub(size(mean_J),index);
    white_r = J(x,y,1);
    white_g = J(x,y,2);
    white_b = J(x,y,3);
    R = J(:,:,1)./white_r;
    G = J(:,:,2)./white_g;
    B = J(:,:,3)./white_b;
    J = cat(3,R,G,B);
end