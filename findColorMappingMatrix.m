function color_matrix = findColorMappingMatrix(oldColors,newColors)
    r_row = oldColors'\newColors(1,:)';
    g_row = oldColors'\newColors(2,:)';
    b_row = oldColors'\newColors(3,:)';
    color_matrix = [r_row';g_row';b_row'];
end