function simg = cropEdge(bimg, s, e)
    [h,w] = size(bimg);
    simg = bimg(round(s*h):round(e*h),round(s*w):round(e*w));
end