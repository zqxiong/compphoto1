function diff = derivativeDiff(image1, image2)
    [h,w] = size(image1);
    image1VD = image1(2:h,:)-image1(1:h-1,:);
    image2VD = image2(2:h,:)-image2(1:h-1,:);
    image1HD = image1(:,2:w)-image1(:,1:w-1);
    image2HD = image2(:,2:w)-image2(:,1:w-1);
    diff = sum(sum(image1VD-image2VD).^2)+sum(sum(image1HD-image2HD).^2);
end