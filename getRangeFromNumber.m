function [ range ] = getRangeFromNumber( number )
%getRangeFromNumber Get range from number
%   Detailed explanation goes here
    roundedScaledNumber = round(number/20);
    range = (roundedScaledNumber*2-4):1:(roundedScaledNumber*2+4);
end

