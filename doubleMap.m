function [xs,ys] = doubleMap(x,y)
    xs = (x*2-2):1:(x*2+2);
    ys = (y*2-2):1:(y*2+2);
end