%% Clean up
% These functions calls clean up the MATLAB environment and close all windows
% open "extra" windows.
clear all
close all


%% Variables
% The next few lines define variables for the locations and types of image files
% we will be reading and writing. You  will likely want to change the input and
% output directories to match you personal environment.
input_dir = 'prokudin-gorsky/';
output_dir = './out';
input_file_ext = 'tif';
output_file_ext = 'tif';
files = dir([input_dir '*.' input_file_ext]);

for file = files'
    range = -10:1:10; % This is the range for tiny images (and pyramid)
                    % For large brute force images the range should be proportionally
                    % scaled up
    leftCrop = 0.15;
    rightCrop = 0.85;

    %% Read image file
    % Here we read the input jpg file into a 3D array of 8-bit integers. Before we
    % start to manipulate this image it is very important that we first convert the
    % integer values into doubles.
    file_name = file.name;
    I = imread([input_dir file_name]);
    I = im2double(I);  

    %% Get image size
    [v_sz, h_sz] = size(I);
    v_sz = floor(v_sz / 3);

    %% Split image into three color channels
    B = I(1:v_sz,:);
    G = I(v_sz+1:2*v_sz,:);
    R = I(2*v_sz+1:3*v_sz,:);

    %% Start of brute force
%     redShift = bruteForceMatch(R,G,range,range,leftCrop,rightCrop);
%     blueShift = bruteForceMatch(B,G,range,range,leftCrop,rightCrop);
%     SR = circshift(R, redShift);
%     SB = circshift(B, blueShift);
    %% Brute force end
    %% Start of pyramid
    gradientR = imgradient(R,'prewitt');
    gradientG = imgradient(G,'prewitt');
    gradientB = imgradient(B,'prewitt');
    SR_Pyramid = pyramidMatch(gradientR, gradientG, 500, range, range,leftCrop,rightCrop);
    SB_Pyramid = pyramidMatch(gradientB, gradientG, 500, range, range,leftCrop,rightCrop);
    SRB_Pyramid = pyramidMatch(gradientR, gradientB, 500, range, range,leftCrop,rightCrop);
    expected_SRB_Pyramid = SR_Pyramid - SB_Pyramid;
    difference = SRB_Pyramid - expected_SRB_Pyramid;
    if sum(abs(difference)) >= 15
        disp(strcat('Manually retouch: /',file.name))
        
        figure
        imshow(I); axis image
        [x, y] = ginput(6);
        redShiftX1 = round(x(2) - x(3));
        redShiftY1 = 0 - (round(y(3) - y(2)) - v_sz);
        blueShiftX1 = round(x(2) - x(1));
        blueShiftY1 = round(y(2) - y(1)) - v_sz;

        redShiftX2 = round(x(5) - x(6));
        redShiftY2 = 0 - (round(y(6) - y(5)) - v_sz);
        blueShiftX2 = round(x(5) - x(4));
        blueShiftY2 = round(y(5) - y(4)) - v_sz;

        redShiftX = mean([redShiftX1 redShiftX2]);
        redShiftY = mean([redShiftY1 redShiftY2]);
        blueShiftX = mean([blueShiftX1 blueShiftX2]);
        blueShiftY = mean([blueShiftY1 blueShiftY2]);

        redXRange = getRangeFromNumber(redShiftX);
        redYRange = getRangeFromNumber(redShiftY);
        blueXRange = getRangeFromNumber(blueShiftX);
        blueYRange = getRangeFromNumber(blueShiftY);
        
        SR_Pyramid = pyramidMatch(gradientR, gradientG, 500, redYRange, redXRange, leftCrop, rightCrop);
        SB_Pyramid = pyramidMatch(gradientB, gradientG, 500, blueYRange, blueXRange, leftCrop, rightCrop);
    end
    SR = circshift(R, SR_Pyramid);
    SB = circshift(B, SB_Pyramid);
    %% End of pyramid
    J = cat(3, cropEdge(SR,0.1,0.9), cropEdge(G,0.1,0.9), cropEdge(SB,0.1,0.9));
    J = whiteBalance(J);
    %% Recoloring Algorithm
%   oldColors = [0.241 0.106 0.567;0.42 0.23 0.40;0.82 0.93 0.96]';
%   newColors = [0.42 0.23 0.40; 0.42 0.23 0.40;0.82 0.93 0.96]';
%   J = recolor(J, oldColors, newColors);
    imwrite(J, [output_dir file_name(1:end-3) output_file_ext]);
end

