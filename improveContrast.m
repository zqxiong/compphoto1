function J = improveContrast(J)
    minimum = min(min(min(J)));
    maximum = max(max(max(J)));
    range = maximum - minimum;
    R = rescale(J(:,:,1),minimum,range);
    G = rescale(J(:,:,2),minimum,range);
    B = rescale(J(:,:,3),minimum,range);
    J = cat(3,R,G,B);
end

