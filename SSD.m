function s = SSD(image1, image2)
    s = sum(sum((image1-image2).^2));
end