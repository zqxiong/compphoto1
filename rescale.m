function I = rescale(I,minimum,range)
    I = (I - minimum).*(1/range);
end