function gradientSSD = gradientSSD(image1, image2)
    %[magnitude1, direction1] = imgradient(image1,'prewitt');
    %[magnitude2, direction2] = imgradient(image2,'prewitt');
    edge1 = edge(image1, 'Canny');
    edge2 = edge(image2, 'Canny');
    %magnitudeSSD = sum(sum((magnitude1-magnitude2).^2))/10^3;
    %directionSSD = sum(sum((direction1-direction2).^2))/10^9;
    edgeSSD = sum(sum((edge1-edge2).^2))/10^4;
    gradientSSD = edgeSSD;
end