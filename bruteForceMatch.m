function minMove = bruteForceMatch(image1,image2,xRange,yRange,leftCrop,rightCrop)
    minSSD = Inf;
    minMove = [0,0];
    [minX, minY, maxX, maxY] = getMinMaxFromRange(xRange, yRange);
    xLength = -minX+maxX+1;
    yLength = -minY+maxY+1;
    z = zeros(xLength,yLength);
    for x = xRange
        for y = yRange
            shiftedImage1 = circshift(image1,[x y]);
            croppedImage1 = cropEdge(shiftedImage1,leftCrop,rightCrop);
            croppedImage2 = cropEdge(image2,leftCrop,rightCrop);
            gradientDiff = SSD(croppedImage1,croppedImage2);
            z(x-minX+1,y-minY+1) = gradientDiff;
            if gradientDiff < minSSD
                minSSD = gradientDiff;
                minMove = [x,y];
            end
        end
    end
%     minMoves = [];
%     zCopy = z;
%     for i=1:1:19
%         [minValueX minValueY] = ind2sub(size(zCopy),find(abs(zCopy-min(min(zCopy))) < 0.0005));
%         zCopy(minValueX,minValueY) = Inf;
%         minMoves = [minMoves [minValueX-1+minX; minValueY-1+minY]];
%     end
%     newZ = zeros(xLength,yLength);
%     for row = minMoves
%         newZ(row(1)-minX+1,row(2)-minY+1) = z(row(1)-minX+1,row(2)-minY+1);
%     end
%     figure
%     stem3(xRange, yRange, newZ)
%     [magnitude1, direction1] = imgradient(z,'prewitt')
%     [minVal minIndex] = min(magnitude1)
%     [val index] = min(minVal)
%     x = index
end