function shift = pyramidMatch(image1, image2, heightThreshold, xRange, yRange, leftCrop, rightCrop)
    [height,~] = size(image1);
    if height < heightThreshold
        shift = bruteForceMatch(image1,image2,xRange,yRange,leftCrop,rightCrop);
    else
        halfImage1 = imresize(image1,0.5);
        halfImage2 = imresize(image2,0.5);
        smallShift = pyramidMatch(halfImage1, halfImage2, heightThreshold, xRange, yRange, leftCrop, rightCrop);
        [doubleXRange,doubleYRange] = doubleMap(smallShift(1),smallShift(2));
        shift = bruteForceMatch(image1,image2,doubleXRange,doubleYRange,leftCrop,rightCrop);
    end
end