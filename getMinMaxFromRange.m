function [ minX, minY, maxX, maxY ] = getMinMaxFromRange( xRange, yRange )
    maxX = 0;
    maxY = 0;
    for x = xRange
        for y = yRange
            maxX = x;
            maxY = y;
        end
    end
    for x = xRange
        minX = x;
        break;
    end
    for y = yRange
        minY = y;
        break;
    end
end

