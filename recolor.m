function J = recolor(J,oldColors, newColors)
    coloring_matrix = findColorMappingMatrix(oldColors,newColors);
    real_red = coloring_matrix(1,:);
    real_green = coloring_matrix(2,:);
    real_blue = coloring_matrix(3,:);
    R = J(:,:,1);
    G = J(:,:,2);
    B = J(:,:,3);
    real_R = real_red(1)*R + real_red(2)*G + real_red(3)*B;
    real_G = real_green(1)*R + real_green(2)*G + real_green(3)*B;
    real_B = real_blue(1)*R + real_blue(2)*G + real_blue(3)*B;
    J = cat(3,real_R,real_G,real_B);
end